﻿use master
GO

DROP DATABASE SDatabase
GO

CREATE DATABASE SDatabase
GO

ALTER DATABASE SDatabase COLLATE SQL_Latin1_General_CP1251_CI_AS

use SDatabase
GO


CREATE TABLE Emploee(
	Logn char(10) NOT NULL PRIMARY KEY,
	Passwd varchar(10) NOT NULL,
	FIO varchar(30),
	Position varchar(30),
)
GO

--Drop table Emploee

INSERT INTO Emploee
VALUES
('abc1','Улица','manager','Andrey Hio'),
('abc2','Улица','manager','Samson Vasulkin'),
('abc3','Улица','admin','Karmedon Asad'),
('abc4','Улица','admin','Marinas Karinas'),
('abc5','Улица','emploee','Tomilich Jay'),
('abc6','Улица','emploee','Madam Oppad'),
('abc7','Улица','emploee','Poit Sid');

select * from Emploee


CREATE TABLE Addrs(
	addrid int NOT NULL PRIMARY KEY,
	addrs varchar(30),
	ax int,
	ay int,
)
GO

--Drop table Addrs

CREATE TABLE Product(
	productid int NOT NULL PRIMARY KEY,
	productinfo varchar(30),
	productsize int,
)
GO

--Drop table Product

CREATE TABLE CellProduct(
	orderid int NOT NULL PRIMARY KEY,
	productid int,
	amount int,
)
GO

--Drop table CellProduct

CREATE TABLE Cell(
	cellid char(10) NOT NULL PRIMARY KEY,
	/*List<CellProduct> ListOfCProduct*/
	cellsize int,
	freesize int,
)
GO

--Drop table Cell

CREATE TABLE Storage(
	strgid char(10) NOT NULL PRIMARY KEY,
	addrid int,
	ssize int,
	flowsize int,
	/*List<Cell> ListOfCell*/
)
GO

--Drop table Storage

CREATE TABLE Client(
	clientid char(10) NOT NULL PRIMARY KEY,
	FIO varchar(30),
	/*ist<Order*/
)
GO

--Drop table Client

CREATE TABLE Ordr(
	orderid char(10) NOT NULL PRIMARY KEY,
	addrid int,
	client varchar(30),
	OrdrDate DateTime,
	MaxDate DateTime,
	/*List<OrderedProduct>*/
	sts varchar(10),
)
GO

--Drop table Ordr

--drop database SDatabase