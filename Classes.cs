﻿using System;
using System.Collections.Generic;

namespace Supply_Stock
{
    static class Classes
    {
        public class Emploee
        {
            private string login, password, position, fio;
            public Emploee(string login, string password, string position, string fio)
            {
                this.login = login;
                this.password = password;
                this.position = position;
                this.fio = fio;
            }
            public bool CheckDataSet(string login, string password)
            {
                if (login==this.login && password==this.password) return true; 
                return false;
            }
            public string GetLID()
            {
                return this.login;  
            }
            public string GetFIO()
            {
                return this.fio;  
            }
            public int GetPasCount()
            {
                return this.password.Length;
            } 
            public string GetPos()
            {
                return this.position;  
            }
            public void SetLogin(string login)
            {
                this.login = login;
            }
            public void SetPassw(string password)
            {
                this.password = password;
            }
            public void SetPosition(string position)
            {
                this.position = position;
            }
            public void SetFIO(string fio)
            {
                this.fio = fio;
            }

        }

        public class Adress
        {
            private string addrid, address;
            private int ax, ay;
            public Adress(string address, int ax, int ay)
            {
                this.address = address;
                this.ax = ax;
                this.ay = ay;
            }
            public void SetAID(string aid)
            {
                this.addrid = aid;
            }
            public string GetAddress()
            {
                return this.address;
            }
            public double FindRange(Adress adress)
            {
                double range = Math.Sqrt(Math.Abs(this.ax - adress.ax) ^ 2 + Math.Abs(this.ay - adress.ay) ^ 2);
                return range;
            }
        }

        public class Adresses
        {
            private List<Adress> ListOfAdress = new List<Adress>();

            public Adress GetAddress(string address)
            {
                for (int i = 0; i < this.ListOfAdress.Count; i++)
                {
                    if (ListOfAdress[i].GetAddress() == address)
                        return ListOfAdress[i];
                }
                return null;
            }
            public List<Adress> GetLOA()
            {
                return this.ListOfAdress;
            }
            public void AddNewAddress(Adress addr)
            {
                for (int i = 0; i < this.ListOfAdress.Count; i++)
                {
                    if (this.ListOfAdress[i].GetAddress() == addr.GetAddress())
                    {
                        throw new Exception();
                    }
                }
                //addr.SetAID(addrid);
                ListOfAdress.Add(addr);
            }
        }

        public class Product
        {
            protected string productid, productinfo;
            protected int productsize;
            public Product(string productinfo, int productsize)
            {
                this.productinfo = productinfo;
                this.productsize = productsize;
            }
            public void SetPID(string pid)
            {
                this.productid = pid;
            }
            public string GetPID()
            {
                return productid;
            }
            public int GetPSize()
            {
                return productsize;
            }
            public string GetPInfo()
            {
                return productinfo;
            }
            public void EditPSize(int productsize)
            {
                this.productsize = productsize;
            }
            public void EditPInfo(string productinfo)
            {
                this.productinfo = productinfo;
            }
        }
        
        public class OrderedProduct : Product
        {
            private int orderedamount; string storageaddress;
            private Product product;

            public OrderedProduct(Product product, int orderedamount, string orderid) : base(product.GetPInfo(), product.GetPSize())
            {
                this.productid = product.GetPID();
                this.orderedamount = orderedamount;
                this.product = product;
            }
            public int GetAmount()
            {
                return this.orderedamount;
            }
            public string GetStorage()
            {
                return this.storageaddress;
            }
            public void SetAmount(int orderedamount)
            {
                this.orderedamount = orderedamount;
            }
            public void SetStorage(string address)
            {
                this.storageaddress = address;
            }
            public Product GetParProduct()
            {
                return this.product;
            }
        }

        public class CellProduct : Product
        {
            private int amount;
            private Product product;
            string orderid;
            public CellProduct(Product product, int amount, string orderid) : base(product.GetPInfo(), product.GetPSize())
            {
                this.product = product;
                this.orderid = orderid;
                this.productid = product.GetPID();
                this.amount = amount;
            }
            public Product GetParProduct()
            {
                return this.product;
            }
            public string GetOID()
            {
                return this.orderid;
            }
            public int GetAmount()
            {
                return this.amount;
            }
            public void SetAmount(int amount)
            {
                this.amount += amount;
            }
        }

        public class Cell
        {
            private int cellsize, freesize;
            private string cellid; List<CellProduct> ListOfCProduct = new List<CellProduct>();
            bool isfree = true;
            public Cell(int cellsize)
            {
                this.cellsize = cellsize;
                this.freesize = cellsize;
                if (freesize != cellsize) isfree = false;
            }
            public void SetCID(string cid)
            {
                this.cellid = cid;
            }
            public string GetCID()
            {
                return this.cellid;
            }
            public List<CellProduct> GetLOCP()
                {
                return this.ListOfCProduct;
                }
            public int GetSize()
            {
                return this.cellsize;
            }
            public bool CheckFreeSpace(int size)
            {
                if (freesize >= size) return true;
                else return false;
            }
            public int GetFreeSpace()
            {
                return this.freesize;
            }
            public CellProduct PlaceProduct(CellProduct CellProduct, string ordrid)
            {
                if (CellProduct.GetPSize() * CellProduct.GetAmount() > this.freesize) return CellProduct;
                if (this.freesize >= (CellProduct.GetPSize() * CellProduct.GetAmount()))
                {
                    this.ListOfCProduct.Add(CellProduct);
                    this.freesize -= CellProduct.GetPSize() * CellProduct.GetAmount();
                    this.isfree = false;
                    return null;
                }
                else
                {
                    CellProduct cp2 = new CellProduct(CellProduct.GetParProduct(),0, ordrid);

                    CellProduct.SetAmount(this.freesize / CellProduct.GetPSize());
                    cp2.SetAmount(CellProduct.GetAmount()-(this.freesize / CellProduct.GetPSize()));
                    this.ListOfCProduct.Add(CellProduct);
                    this.freesize -= CellProduct.GetPSize() * CellProduct.GetAmount();
                    this.isfree = false;

                   return cp2;
                }
                    
            }
            public void RemoveProduct(CellProduct CellProduct, int amount)
            {
                if (CellProduct.GetAmount() < amount) throw new Exception();
                if (CellProduct.GetAmount() == amount) this.ListOfCProduct.Remove(CellProduct);
                this.freesize += CellProduct.GetPSize()*(CellProduct.GetAmount()-amount);
                amount = 0 - amount;
                CellProduct.SetAmount(amount);
            }
            public bool FindProduct(string productid)
            {
                for (int j = 0; j < this.ListOfCProduct.Count; j++)
                    if (this.ListOfCProduct[j].GetPID() == productid) return true;
                return false;
            }
        }

        public class Storage
        {
            private string strgid;
            private Adress address;
            private int ssize, flowsize; //общий объём склада, пустой объём склада (не построенные полки)
            private List<Cell> ListOfCell = new List<Cell>();
            public Storage(Adress address, int ssize)
            {
                this.address = address;
                this.ssize = ssize;
                this.flowsize = ssize;
            }
            public Adress GetAddress()
            {
                return this.address;
            }
            public string GetSID()
            {
                return this.strgid;
            }
            public void SetSID(string sid)
            {
                this.strgid = sid;
            }
            public List<Cell> GetLOC()
            {
                return this.ListOfCell;
            }
            public void AddCell(Cell cell)
            {
                
                if (this.flowsize > cell.GetSize())
                {          
                    //cell.SetCID(cellid);
                    this.ListOfCell.Add(cell);
                    this.flowsize -= cell.GetSize();
                }
                else throw new Exception();
            }
            public void RemoveCell(Cell cell)
            {
                if (cell.GetFreeSpace() == cell.GetSize())
                {
                    this.ListOfCell.Remove(cell);
                }
                else throw new Exception();
            }
            public Cell GetCell(string cellid)
            {
                for (int j = 0; j < this.ListOfCell.Count; j++)
                    if (this.ListOfCell[j].GetCID() == cellid)
                        return this.ListOfCell[j];
                return null;
            }
            public bool CheckFreeSpace(int amount)
            {
                int freespace=this.flowsize;
                for (int i = 0; i > this.ListOfCell.Count; i++)
                {
                    freespace+=this.ListOfCell[i].GetFreeSpace();
                    if(freespace>=amount) return true;
                }
                return false;
            }
            public void EditStorage(Storage strg)
            {
                this.address = strg.address;
                this.ListOfCell = strg.ListOfCell;
            }
            public Cell FindFreeCell(int freespace)
            {
                for (int j = 0; j < this.ListOfCell.Count; j++)
                    if (this.ListOfCell[j].CheckFreeSpace(freespace) == true)
                        return this.ListOfCell[j];
                return null;
            }
            public Order PlaceOrder(Order ordr)
            {
                var cell = this.FindFreeCell(ordr.GetVolume());
                if (cell != null)
                {
                    for (int i = 0; i < ordr.GetLOP().Count; i++)
                    {
                        var cp = new CellProduct(ordr.GetLOP()[i].GetParProduct(), ordr.GetLOP()[i].GetAmount(), ordr.GetOID());
                        cell.PlaceProduct(cp, ordr.GetOID());
                    }
                    ordr.SetDelAdress(this.address );
                    return null;
                }
                else
                {
                    var neworder = new Order(ordr.GetClient());
                    var newlst = ordr.GetLOP();

                    for (int i = 0; i < newlst.Count; i++)
                    {
                        for (int j=0; j<this.ListOfCell.Count;j++)
                        {
                            var newcell = this.FindFreeCell(newlst[i].GetAmount()*newlst[i].GetPSize());

                            if (neworder.GetVolume() > this.flowsize) break; //Место на складе закончилось, необходимо переместить заказ на другой склад
                            if (neworder.GetVolume() <= this.flowsize) throw new Exception(); //Место в существующих ячейках закончилось, необходимо добавить новые

                            if (newcell == null)
                            {
                                neworder.AddProductInOrder(new OrderedProduct(newlst[i].GetParProduct(), newlst[i].GetAmount(), neworder.GetOID()), newlst[i].GetAmount());
                            } 
                            var pp = newcell.PlaceProduct(new CellProduct(newlst[i].GetParProduct(), newlst[i].GetAmount(), neworder.GetOID()), neworder.GetOID());
                            
                            newlst.Remove(newlst[i]);
                        }

                    }
                    return neworder; //если весь заказ не помещается в одну ячейку
                }  
                
            }
        }

        public class Storages
        {
            private List<Storage> ListOfStorage = new List<Storage>();
            public Storages()
            {

            }
            public Storage GetStorage(string storageaddr)
            {
                for (int i = 0; i < this.ListOfStorage.Count; i++)
                {
                    if (ListOfStorage[i].GetAddress().GetAddress() == storageaddr)
                        return ListOfStorage[i];
                }
                return null;
            }
            public void AddNewStorage(Storage st)
            {
                
                for (int i = 0; i < this.ListOfStorage.Count; i++)
                    if (this.ListOfStorage[i].GetAddress() == st.GetAddress())
                    {
                        throw new ArgumentException();
                    }
                //st.SetSID(strgid);
                ListOfStorage.Add(st);
            }

            public Adress FindFreeStorage(int freespace)
            {
                for (int i = 0; i < this.ListOfStorage.Count; i++)
                    if (this.ListOfStorage[i].FindFreeCell(freespace) != null)
                        return ListOfStorage[i].GetAddress();
                return null;
            }

            public Adress FindFreeNearestStorage(Adress address, Adresses ads, Storages stt, int amount)
            {
                double rg = 0;
                var lst = ads.GetLOA();
                var ltt = stt.GetLOS();
                while (lst.Count>0)
                {
                    for (int j = 0; j < ltt.Count; j++)
                    {
                        if (Math.Abs(lst[j].FindRange(address)) < rg && ltt[j].CheckFreeSpace(amount)==true) rg = Math.Abs(lst[j].FindRange(address));
                    }

                    for (int j = 0; j < lst.Count; j++)
                    {
                        if (Math.Abs(lst[j].FindRange(address)) == rg) return lst[j];
                    }
                    break;
                }
                return address;
            }
            public List<Storage> GetLOS()
            {
                return this.ListOfStorage;
            }
        }

        public class LOPr
        {
            private List<Product> ListOfProduct;
            List<Product> GetLOPr()
            {
                return this.ListOfProduct;
            }
            void AddProductInList(Product prdct)
            {
                this.ListOfProduct.Add(prdct);
               //prdct.SetPID(productid);
            }
        }
        public class LOCl
        {
            private List<Client> ListOfClient;
            List<Client> GetLOCl()
            {
                return this.ListOfClient;
            }
            void AddProductInList(Client clnt)
            {
                this.ListOfClient.Add(clnt);
                //clnt.SetCLID(clientid);
            }
        }
        public class Order
        {
            private string orderid;
            private Adress deliveryaddress;
            private Client client;
            private DateTime OrdrDate = DateTime.Today; DateTime MaxDate = DateTime.Today.AddDays(10);
            private List<OrderedProduct> ListOfOProduct = new List<OrderedProduct>();
            private string status;
            public Order(Client client)
            {
                this.status = "New";
                this.client = client;
                client.AddNewOrder(this);
            }
            public DateTime GetOrdrDate()
            {
                return this.OrdrDate;
            }
            public DateTime GetMaxDate()
            {
                return this.MaxDate;
            }
            public void SetOID(string oid)
            {
                this.orderid = oid;
            }
            public string GetOID()
            {
                return this.orderid;
            }
            public void SetDelAdress(Adress adress)
            {
                this.deliveryaddress = adress;
            }
            public void SetStatus(string status)
            {
                this.status = status;
            }
            public Adress GetDelAddr()
            {
                return this.deliveryaddress;
            }
            public List<OrderedProduct> GetLOP()
            {
                return this.ListOfOProduct;
            }
            public int GetVolume()
            {
                int vlm=0;
                if (this.ListOfOProduct.Count==0) return 0;
                for (int i = 0; i < this.ListOfOProduct.Count; i++)
                    vlm += (ListOfOProduct[i].GetAmount() * ListOfOProduct[i].GetPSize());
                return vlm;
            }
            public void AddProductInOrder(OrderedProduct product, int amount)
            {
                for (int i = 0; i < this.ListOfOProduct.Count; i++)
                    if(this.ListOfOProduct[i].GetPID() == product.GetPID())
                    {
                        product.SetAmount(amount);
                    }
               this.ListOfOProduct.Add(product);
            }

            internal Client GetClient()
            {
                return this.client;
            }
        }
        public class Client
        {
            private string fio, clientid;
            private List<Order> ListOfOrder = new List<Order>();
            public Client(string fio)
            {
                this.fio = fio;
            }
            public void SetCLID(string clid)
            {
                this.clientid = clid;
            }
            public void AddNewOrder(Order ordr)
            {
                //ordr.SetOID(orderid);
                this.ListOfOrder.Add(ordr);
            }
            public Order FindOrder(string orderid)
            {
                for (int i = 0; i < this.ListOfOrder.Count; i++)
                    if (this.ListOfOrder[i].GetOID() == orderid) return this.ListOfOrder[i];
                return null;
            }
        }

    }
}