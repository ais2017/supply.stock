﻿using NUnit.Framework;
using System;
using static Supply_Stock.Classes;
using static Supply_Stock.FuncData;

namespace Supply_Stock
{
    public class FuncTests
    {
        static DTB dtb = new DTB();
        static OTS ots = new OTS();

        [Test]
        public void Logining()
        {
            string login = "login", password = "password";
            if (dtb.GetEmp(login) != null) Assert.AreEqual(dtb.GetEmp(login).CheckDataSet(login, password), true);

            login = "logen"; password = "pasord";
            if (dtb.GetEmp(login)!=null) Assert.AreEqual(dtb.GetEmp(login).CheckDataSet(login, password), false);
        }
        [Test]
        public void OpenLOE()
        {
            Assert.AreEqual(dtb.GetLOE().Count, 3);
        }
        [Test]
        public void OpenEmp()
        {
            Assert.AreEqual(dtb.GetEmp("login").GetLID(), "login");
        }
        [Test]
        public void EditEmploee()
        {
            dtb.SetFIO("login", "Familkin Sussann");
            Assert.AreEqual(dtb.GetEmp("login").GetFIO(), "Familkin Sussann");
        }
        [Test]
        public void NewEmploee()
        {
            var emp = new Emploee("kassin", "kassin", "manager", "Kassinar Dott");
            dtb.NewEmploee(emp);
            Assert.AreEqual(dtb.GetEmp("kassin").GetFIO(), "Kassinar Dott");
        }
        [Test]
        public void OpenStorages()
        {
            Assert.AreEqual(dtb.GetStorages().GetStorage("adress").GetAddress().GetAddress(), "adress");
        }
        [Test]
        public void EditStorage()
        {
            dtb.GetStorages().GetStorage("adress").SetSID("newseed");
            Assert.AreEqual(dtb.GetStorages().GetStorage("adress").GetSID(), "newseed");
        }
        [Test]
        public void GetOrder()
        {
            var ordr = ots.GetOrder("order1");
            Assert.AreEqual(ordr.GetOID(), "order1");
        }
        [Test]
        public void AddProductInOrder()
        {
            var o = ots.GetLORD()[1];
            var p = new Product("kartoha", 12);
            var op = new OrderedProduct(p,10, o.GetOID());
            o.AddProductInOrder(op, 10);
            Assert.AreEqual(o.GetLOP()[0].GetPInfo(), "kartoha");
        }
        [Test]
        public void PlaceOrder()
        {
            var o = ots.GetLORD()[1];
            var s = dtb.GetStorages().GetStorage(dtb.GetStorages().FindFreeStorage(o.GetVolume()).GetAddress());
            Assert.AreEqual(s.PlaceOrder(o),null);
        }
        [Test]
        public void SplitOrder()
        {
            var ost = dtb.GetLORD()[1];//new Order(new Client("mamkin homyak"));
            //ost.SetOID("homyak homyak");
            //ost.AddProductInOrder(new OrderedProduct(new Product("homyachii corm", 12000), 12000, ost.GetOID()), 12000);
            for (int i = 0; i < dtb.GetStorages().GetLOS().Count; i++)
                if (ost != null)
                {
                    {
                        var addr = dtb.GetStorages().FindFreeStorage(ost.GetVolume());

                        if (addr != null) ost = dtb.GetStorages().GetStorage(addr.GetAddress()).PlaceOrder(ost);
                        else throw new Exception(); //НЕ ВЛАЗИЕТ;
                    }
                    if (i == dtb.GetStorages().GetLOS().Count && ost != null) throw new Exception(); //НЕ ВЛАЗИЕТ
                }
        }
        [Test]
        public void AddNewProduct()
        {
            var p = new Product("mandarin", 5);
            p.SetPID("prodid");

            dtb.AddNewProduct(p);
            Assert.AreEqual(dtb.GetProduct("prodid"), p);
        }
        [Test]
        public void OrderManagment()
        {
            Order o=null;
            for (int i = 0; i < dtb.GetLORD().Count; i++)
                if (dtb.GetLORD()[i].GetMaxDate() == DateTime.Today)
                    DropOrder(dtb.GetLORD()[i].GetOID(), "expired");

            for (int i = 0; i < dtb.GetLORD().Count; i++)
                if (dtb.GetLORD()[i].GetMaxDate() == DateTime.Today)
                     o = dtb.GetLORD()[i];

            Assert.AreEqual(o, null);
        }
        [Test]
        [TestCase("order3", "moved")]
        public void DropOrder(string ordrid, string status)
        {
            for (int i = 0; i < dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC().Count; i++)
                for (int j = 0; j < dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP().Count; j++)
                    if (dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP()[j].GetOID() == ordrid)
                        dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP().Remove(dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP()[j]);
            dtb.GetOrder(ordrid).SetStatus(status);


        }
        [Test]
        public void OrderIssurance()
        {
            DropOrder("order2", "issued");
        }

        [Test]
        public void ProductLogistic()
        {
            DropOrder("order2", "moved");
        }

        static public class Program
        {
            static void Main(string[] args) {
            while (true)
            {
                Console.ReadLine();
            }
        } } 
    }
}