﻿using System;
using System.Linq;
using static Supply_Stock.Classes;
using static Supply_Stock.FuncData;

namespace Supply_Stock
{
    //struct 
    public class Functions
    {
        
       /*//Установить локальный обьект базы
        // БП - аутентификация пользователя
        bool autotenf(){
            string login, passwd;
            // получить из клиентиского интерфейса login и passwd
            // сформировать обращение к бд
            bool t = query(login, passwd); //внутри создать дтб, и проверить данные
            return t;
        }

        // БП - перемещение товара
        void move()
        {
            if(autotenf()){

            }
            else{
                answer("ошибка аут...");
            }
        }*/
        void Logining(DTB dtb, string login, string password)
        {
            int attempt = 5;
            while (attempt != 0)
                if (dtb.GetEmp(login).CheckDataSet(login, password) != true) attempt -= 1;
                else break; //Показать систему 

            //Блокировка на 10 минут
        }

//Administration-------------------------------------------------------------------------------------------------
        void OpenLOE(DTB dtb)
        {
            //показать список сотрудников

            for (int i = 0; i < dtb.GetLOE().Count; i++)
            {
                dtb.GetLOE()[i].GetFIO();
            }
           //отобразить в интерфейсе данные сотрудников
        }
        void OpenEmp(DTB dtb,string login)
        {
            //запускается при двойном щелчке на сотрудника
            dtb.GetEmp(login);
            //отобразить в интерфейсе данные сотрудника
        }
        void EditEmploee(DTB dtb, string login, string oldpassword, string password, string position, string fio)
        {
            //запускается при нажатии на кнопку "Изменить сотрудника" (сохранить)
            bool fflg=false, pflg=false, sflg=false;

            //отследить какое поле было изменено и сетить только его
            if (fflg==true) dtb.SetFIO(login, fio);
            //чтобы поменять пароль надо ввести старый пароль
            if(pflg==true && dtb.GetEmp(login).CheckDataSet(login, oldpassword)==true) dtb.SetPassw(login, password);
            if(sflg==true) dtb.SetPosition(login, position);

            if(fflg==false && pflg==false && sflg==false) throw new Exception(); //ничего не было изменено
        }

        void NewEmploee(DTB dtb, string login, string password, string fio, string position)
        {
            //запускается при нажатии на кнопку "Создать сотрудника" при просмотре (сохранить)
            dtb.NewEmploee(new Emploee(login, password, fio, position)); //создать данные в бд
        }

        void OpenStorages(DTB dtb)
        {
            //показать список складов
            dtb.GetStorages();
            //отобразить в интерфейсе данные складов
        }

        void EditStorage(DTB dtb, Adress adress, int ssize)
        {
            //запускается при двойном щелчке на склад
            var strg = new Storage(adress, ssize);
            dtb.GetStorages().GetStorage(adress.GetAddress()).EditStorage(strg);
        }

        //----------------------------------------------------------------------------------------------------------------
        //Order Managment-------------------------------------------------------------------------------------------------

        void GetOrder(DTB dtb, string oid)
        {
            //получение списка продуктов из заказа
            dtb.GetOrder(oid).GetLOP(); 
            //пользователь проверяет все ли товары там есть
        }

        void AddProductInOrder(DTB dtb, string oid, string productid, int amount)
        {
            //добавить продукт в заказ (выбор из списка существующих продуктов)
            var op = new OrderedProduct(dtb.GetProduct(productid), amount, oid);
            dtb.GetOrder(oid).AddProductInOrder(op, amount); 
        }

        void PlaceOrder(DTB dtb, string ordrid, bool sflg, bool nflg)
        {
            Adress addr = null;

            //При флаге разместить на ближайшем складе
            if (nflg == true) addr = dtb.GetStorages().FindFreeNearestStorage(dtb.GetOrder(ordrid).GetDelAddr(), dtb.GetAdresses(), dtb.GetStorages(), dtb.GetOrder(ordrid).GetVolume()); //(выдаёт адрес склада, в который поместится весь заказ)

            //Поиск свободного места для размещения товаров (выдаёт адрес первого склада, в который поместится весь заказ)
            if (nflg == false) addr = dtb.GetStorages().FindFreeStorage(dtb.GetOrder(ordrid).GetVolume());

            //При флаге разместить заказ на нескольких складах (не поместился на один)
            if (sflg == true && addr == null) SplitOrder(dtb, ordrid);
            else if (addr==null) throw new Exception(); //Нет склада, на котором поместится весь заказ

            dtb.GetStorages().GetStorage(addr.GetAddress()).PlaceOrder(dtb.GetOrder(ordrid));
        }

        void SplitOrder(DTB dtb, string ordrid)
        {
            var ost = dtb.GetOrder(ordrid);
            for (int i=0; i< dtb.GetStorages().GetLOS().Count;i++)
                if(ost != null) {
                    {
                        var addr = dtb.GetStorages().FindFreeStorage(ost.GetVolume());

                        if (addr != null) ost = dtb.GetStorages().GetStorage(addr.GetAddress()).PlaceOrder(ost);
                        else throw new Exception(); //НЕ ВЛАЗИЕТ;
                    }
                    if (i == dtb.GetStorages().GetLOS().Count && ost != null) throw new Exception(); //НЕ ВЛАЗИЕТ
            }
        } 
        void AddNewProduct(DTB dtb, string productinfo, int productsize)
        {
            //Если нужного продукта вообще вообще не существует
            var prdct = new Product(productinfo, productsize);
            dtb.AddNewProduct(prdct);
        }

        void OrderManagment(DTB dtb, Order ordrid)
        {
            //Запускается по таймеру
            //Проверить дату заказа, если закончилось место и дропнуть
            for (int i = 0; i < dtb.GetLORD().Count; i++)
                if (dtb.GetLORD()[i].GetMaxDate() == DateTime.Today)
                    DropOrder(dtb, dtb.GetLORD()[i].GetOID(), "expired");
                
            
        }

        void OrderIssuance(DTB dtb, string ordrid)
        {
            //Выдача производится сотрудником ОДНОГО склада
            //Остальные заказы (части заказа) клиента, размещённые на других складах, хранятся в списке его неисполненных заказов
            DropOrder(dtb, ordrid, "issued");
        }

        void DropOrder(DTB dtb, string ordrid, string status)
        {
            //подключение к базе
            //экземпляр заказа чтобы знать какие товары
            //изменение статуса
            //экземпляры склада
            //сохранение в базе
            for (int i=0; i < dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC().Count; i++)
                for (int j = 0; j < dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP().Count; j++)
                    if (dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP()[j].GetOID() == ordrid)
                        dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP().Remove(dtb.GetStorages().GetStorage(dtb.GetOrder(ordrid).GetDelAddr().GetAddress()).GetLOC()[i].GetLOCP()[j]);
            dtb.GetOrder(ordrid).SetStatus(status);
        }
//----------------------------------------------------------------------------------------------------------------
//ProductLogistic-------------------------------------------------------------------------------------------------

        void ProductLogistic(DTB dtb, string orderid, Adress newadress)
        {
            DropOrder(dtb, orderid, "moving");
            //Перемещение заказа на другой пункт выдачи (выбирает менеджер)
            
            if (dtb.GetStorages().GetStorage(newadress.GetAddress()).CheckFreeSpace(dtb.GetOrder(orderid).GetVolume()) == true)
                dtb.GetStorages().GetStorage(newadress.GetAddress()).PlaceOrder(dtb.GetOrder(orderid));
            else throw new Exception(); //Нет места на этом складе

            dtb.GetOrder(orderid).SetDelAdress(newadress);
            dtb.GetOrder(orderid).SetStatus("new");
        }
    }
}
