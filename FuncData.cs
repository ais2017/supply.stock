﻿using System.Collections.Generic;
using static Supply_Stock.Classes;

namespace Supply_Stock
{
    public class FuncData
    {
        public class DTB //Dstabase
        {
            //компонент БД принимает и возвращает экземпляр класса (для гетеров и сетеров)
            List<Emploee> LOE=new List<Emploee>();
            Emploee _tEmploee1 = new Emploee("login", "password", "manager", "Familkin Ivan Andreevich");
            Emploee _tEmploee2 = new Emploee("logen", "password", "manager", "Familkin Dmitry Andreevich");

            static Client _tClient = new Client("Familkin Andrey Vitalich");

            static OTS ots = new OTS();
            static Order _tOrder = new Order(_tClient);

            static Adress _tAdress = new Adress("adress", 5, 2);
            static Adress _tAdr = new Adress("adress 1879", 18, 79);
            static Adress _tAdress1 = new Adress("adress1", 0, 0);
            static Adress _tAdressSt = new Adress("Logisticheskaya ulica, dom 5", 12, 0);

            List<Product> LOP=new List<Product>();
            static Product _tProductBig = new Product("Product", 500);
            static Product _tProduct1 = new Product("Komashka", 15);
            static Product _tProduct2 = new Product("Kartoshka", 12);

            CellProduct _tCellProduct = new CellProduct(_tProduct1, 3, _tOrder.GetOID());
            CellProduct _tCellProductBig = new CellProduct(_tProductBig, 1, _tOrder.GetOID());

            OrderedProduct _tOrderedProduct = new OrderedProduct(_tProduct2, 20, _tOrder.GetOID());

            Cell _tCell1 = new Cell(100);
            Cell _tCell2 = new Cell(800);
            Cell _tCell3 = new Cell(500);

            Adresses _tAdresses = new Adresses();
            Storages _tStorages = new Storages();

            Storage _tStorage = new Storage(_tAdress, 12000);
            Storage _tStorageSmall = new Storage(_tAdress1, 300);
            Storage DefSt = new Storage(_tAdr, 10000);

            public DTB()
            {                
                _tCell1.PlaceProduct(_tCellProduct, _tOrder.GetOID());
                _tCell2.PlaceProduct(_tCellProductBig, _tOrder.GetOID());

                _tOrder.AddProductInOrder(_tOrderedProduct, 5);

                this.LOE.Add(this._tEmploee1);
                this.LOE.Add(this._tEmploee2);

                this.LOP.Add(_tProduct1);
                this.LOP.Add(_tProduct2);
                this.LOP.Add(_tProductBig);

                _tStorage.AddCell(_tCell1);
                _tStorage.AddCell(_tCell2);
                _tStorage.AddCell(_tCell3);

                _tStorageSmall.AddCell(_tCell1);
                DefSt.AddCell(_tCell1);

                _tStorages.AddNewStorage(_tStorage);
                _tStorages.AddNewStorage(_tStorageSmall);
                _tStorages.AddNewStorage(DefSt);

                _tAdresses.AddNewAddress(_tAdress);
                _tAdresses.AddNewAddress(_tAdressSt);
                _tAdresses.AddNewAddress(_tAdress1);
                _tAdresses.AddNewAddress(_tAdr);
            }
            internal Product GetProduct(string pid)
            {
                for(int i=0; i<LOP.Count; i++)
                    if(LOP[i].GetPID()==pid) return LOP[i];
                return null;
            }
            internal void AddNewProduct(Product prdct)
            {
                this.LOP.Add(prdct);
            }
            public void SetFIO(string lid, string fio)
            {
                for(int i=0; i<LOE.Count; i++)
                    if(LOE[i].GetLID()==lid) LOE[i].SetFIO(fio);
            }
            public void SetPosition(string lid, string position)
            {
                for (int i = 0; i < LOE.Count; i++)
                    if (LOE[i].GetLID() == lid) LOE[i].SetPosition(position);
            }
            public void SetPassw(string lid, string password)
            {
                for (int i = 0; i < LOE.Count; i++)
                    if (LOE[i].GetLID() == lid) LOE[i].SetPassw(password);
            }
            internal Emploee GetEmp(string login)
            {
                for (int i = 0; i < LOE.Count; i++)
                    if (LOE[i].GetLID() == login) return LOE[i];
                return null;
            }
            internal void NewEmploee(Emploee emp)
            {
                LOE.Add(emp);
            }
            internal List<Emploee> GetLOE()
            {
               return this.LOE;
            }
            internal List<Product> GetLOP()
            {
               return this.LOP;
            }
            internal Order GetOrder(string oid)
            {
                return ots.GetOrder(oid);
            }
            internal List<Order> GetLORD()
            {
                return ots.GetLORD();
            }
            internal Storages GetStorages()
            {
                return this._tStorages;
            }
            internal Adresses GetAdresses()
            {
                return this._tAdresses;
            }
        }

    }
}