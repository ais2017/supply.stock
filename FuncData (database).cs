﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using static Supply_Stock.Classes;

namespace Supply_Stock
{
    class FuncData
    {
        static SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
        static SqlConnection connection = new SqlConnection(builder.ConnectionString);

        public class DTB
        {
            DTB()
                {
                    builder.DataSource= @"(LocalDB)\MSSQLLocalDB;AttachDbFilename='SDatabase.mdf'"; 
                }
           public Emploee GetEmp(string login)
            {
                var emp = new Emploee("","","","");

                connection.Open();

                var getlog = new SqlCommand("SELECT Login from Emploee where Login ='" + login + "'", connection);
                emp.SetLogin(getlog.ExecuteScalar().ToString());
                var getpasswd = new SqlCommand("SELECT Passwd from Emploee where Login ='" + login + "'", connection);
                emp.SetPassw(getpasswd.ExecuteScalar().ToString());
                var getpos = new SqlCommand("SELECT Position from Emploee where Login ='" + login + "'", connection);
                emp.SetPosition(getpos.ExecuteScalar().ToString());
                var getfio = new SqlCommand("SELECT FIO from Emploee where Login ='" + login + "'", connection);
                emp.SetFIO(getfio.ExecuteScalar().ToString());

                connection.Close();

                return emp;
            }

            public List<Emploee> GetLOE()
            {
                var LOE = new List<Emploee>();

                connection.Open();

                var getlog = new SqlCommand("SELECT * from Emploee", connection);

                using (SqlDataReader dr = getlog.ExecuteReader(CommandBehavior.CloseConnection))
                {
                    //цикл по всем столбцам полученной в результате запроса таблицы
                    for (int i = 0; i < dr.FieldCount; i++) /*метод GetName() класса SqlDataReader позволяет получить имя столбца по номеру, который передается в качестве параметра, данному методу и озночает номер столбца в таблице(начинается с 0) */
                        
                        while (dr.Read())
                        {
                            var log = dr.GetValue(1).ToString();
                            var pas = dr.GetValue(2).ToString();
                            var pos = dr.GetValue(3).ToString();
                            var fio = dr.GetValue(4).ToString();

                            var emp = new Emploee(log, pas, pos, fio);
                            LOE.Add(emp);
                        }

                    connection.Close();
                return LOE;
            }
/*
            this.addrid = "A" + Convert.ToString(GetHashCode());
            this.cellid = "C" + Convert.ToString(GetHashCode());
            this.strgid = "S" + Convert.ToString(GetHashCode());
            this.prdtid = "P" + Convert.ToString(GetHashCode());
            this.clntid = "U" + Convert.ToString(GetHashCode());
            this.ordrid = "O" + Convert.ToString(GetHashCode());*/
        }
    }
}
