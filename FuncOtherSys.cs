﻿using System.Collections.Generic;
using static Supply_Stock.Classes;

namespace Supply_Stock
{
    public class OTS
    {
        List<Order> LORD = new List<Order>();

        public OTS()
        {
            //Connection String to Order System
            var c = new Client("Hassan Kassan");
            var d = new Client("Mermen Kassan");
            var p = new Order(c);
            p.SetOID("order1");
            LORD.Add(p);

            p = new Order(d);
            p.SetOID("order2");
            LORD.Add(p);

            p = new Order(c);
            p.SetOID("order3");
            p.SetDelAdress(new Adress("adress 1879", 18, 79));
            LORD.Add(p);
        }
        internal List<Order> GetLORD()
        {
            return this.LORD;
        }
        internal Order GetOrder(string oid)
        {
            for (int i = 0; i < this.LORD.Count; i++)
                if (this.LORD[i].GetOID() == oid) return this.LORD[i];
            return null; 
        }
        internal void SetLORD(List<Order> lord)
        {
            for(int i=0;i<this.LORD.Count;i++)
                for(int j = 0; j < lord.Count; j++)
                {
                    if (this.LORD[i].GetOID() == lord[j].GetOID()) this.LORD[i] = lord[j];
                    else this.LORD.Add(lord[j]);
                }
        }
    }
}
