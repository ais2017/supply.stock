﻿using Microsoft;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using static Supply_Stock.Classes;

namespace Supply_Stock
{
    [TestFixture]
    public class MethodTest
    {
        private Emploee _tEmploee = new Emploee("login", "password", "manager", "Familkin Ivan Andreevich");
        static private Client _tClient = new Client("Familkin Andrey Vasilevich");
        static private Order _tOrder = new Order(_tClient);
        static private Adress _tAdress = new Adress("adress",5,2);
        static private Adress _tAdress1 = new Adress("adress1", 0, 0);  
        static private Product _tProduct = new Product("product", 5);
        static private Product _tProductBig = new Product("product", 500);
        static private CellProduct _tCellProduct = new CellProduct(_tProduct, 3, _tOrder.GetOID());
        static private CellProduct _tCellProductBig = new CellProduct(_tProductBig, 20, _tOrder.GetOID());
        private OrderedProduct _tOrderedProduct = new OrderedProduct(_tProduct, 20, _tOrder.GetOID());
        private Cell _tCell = new Cell(15);
        private Storage _tStorage = new Storage(_tAdress, 12000);
        private Adresses _tAdresses = new Adresses();
        private Storages _tStorages = new Storages();
        
        

        public MethodTest()
        {

        }

        [Test]
        public void Logining()
        {
            string login = "login", password = "password";
            Assert.AreEqual(_tEmploee.CheckDataSet(login, password) ,true);

            login = "ligin"; password = "pasord";

            Assert.AreEqual(_tEmploee.CheckDataSet(login, password), false);
        }

        [Test]
        public void tAdress()
        {
            var result = _tAdress.FindRange(_tAdress1);

            Assert.That(result, Is.EqualTo(Math.Sqrt(Math.Abs(5^2+2^2))));
            Assert.That(result, Is.EqualTo(Math.Sqrt(Math.Abs(5^2+2^2))));
        }

        [Test]
        public void AddNewAddress()
        {
            var addr = _tAdress;
            _tAdresses.AddNewAddress(addr);

            Assert.Null(_tAdresses.GetAddress("assert"));
            Assert.AreEqual(_tAdresses.GetAddress(_tAdress.GetAddress()).GetAddress(), addr.GetAddress());

            Assert.Throws<Exception>(() => _tAdresses.AddNewAddress(addr));
        }


        [Test]
        public void CheckFreeSpace()
           {
            Assert.AreEqual(_tCell.CheckFreeSpace(10),true);
            Assert.AreEqual(_tCell.CheckFreeSpace(20), false);
            }  
      
        [Test]
        public void PlaceProduct()
           {
             var res1=_tCell.PlaceProduct(_tCellProduct, _tOrder.GetOID());
             Assert.AreEqual(_tCell.FindProduct(_tCellProduct.GetPID()), true);
             Assert.Null(res1);

             
             var res2 = _tCell.PlaceProduct(_tCellProductBig, _tOrder.GetOID());
             Assert.AreEqual(_tCell.FindProduct(_tCellProduct.GetPID()), true);
             Assert.NotNull(res2);

             var res3 = _tCell.PlaceProduct(res2, _tOrder.GetOID());
             Assert.AreEqual(res2, res3);
        }

        [Test]
        public void RemoveProduct()
           {
            int ish = _tCellProduct.GetAmount();

            _tCell.RemoveProduct(_tCellProduct,2);
            Assert.AreEqual(_tCellProduct.GetAmount(), ish-2);

            _tCell.RemoveProduct(_tCellProduct, _tCellProduct.GetAmount());
            Assert.AreEqual(_tCell.FindProduct(_tCellProduct.GetPID()), false);

            Assert.Throws<Exception>(() =>_tCell.RemoveProduct(_tCellProduct, _tCellProduct.GetAmount()+10));

        }

          [Test]
           public void AddCell()
           {
            var cell = new Cell(28);

            Assert.Null(_tStorage.GetCell("assert"));

            _tStorage.AddCell(cell);
            Assert.AreEqual(_tStorage.GetCell(cell.GetCID()), cell);
        }

        [Test]
        public void RemoveCell()
        {
            var cell = new Cell(28);

            _tStorage.AddCell(cell);
            _tStorage.RemoveCell(cell);
            Assert.AreEqual(_tStorage.GetCell(cell.GetCID()), null);
        }

        [Test]
        public void FindFreeCell()
        {
            var st = new Storage(new Adress("addr", 8, 12), 500);
            var cell = new Cell(25);
            st.AddCell(cell);

            Assert.AreEqual(st.FindFreeCell(20), cell);
        }

        [Test]
        public void AddNewStorage()
        {
            var st = new Storage(new Adress("adress for new", 7, 0),2800);

            _tStorages.AddNewStorage(st);

            Assert.Null(_tStorages.GetStorage("assert"));
            Assert.AreEqual(_tStorages.GetStorage(st.GetSID()), st);
        }
        public void FindFreeStorage()
        {
            var sts = new Storages();
            var st = new Storage(new Adress("addr", 8, 12), 500);
            sts.AddNewStorage(st);

            Assert.AreEqual(sts.FindFreeStorage(20), st);
        }

        public void FindNearestStorage()
        {
            _tAdresses.AddNewAddress(_tAdress);
            _tAdresses.AddNewAddress(_tAdress1);
            Assert.AreEqual(_tStorages.FindFreeNearestStorage(_tAdress, _tAdresses, _tStorages, 29), 29);
        }

        [Test]
        public void AddProductInOrder()
        {
            var pr = _tOrderedProduct; bool result=false;
            _tOrder.AddProductInOrder(pr,1);
            for(int i = 0; i < _tOrder.GetLOP().Count; i++)
            {
                if (_tOrder.GetLOP()[i].GetPID() == pr.GetPID())
                {
                    result = true;
                    break;
                }
            }
            Assert.AreEqual(result, true);

        }

       [Test]
       public void FindOrder()
       {
            _tClient.AddNewOrder(_tOrder);
            Assert.AreEqual(_tClient.FindOrder(_tOrder.GetOID()), _tOrder);  
       }


        static public void Main(string[] args)
        {
            DTB dtb; //хранит данные для подключения  к бд.
            while(true){
                Console.ReadLine();
                if op1
                    move();

        }
        }
}

